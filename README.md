# alpine-nodejs-docker-hub-api

#### [alpine-x64-nodejs-docker-hub-api](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-nodejs-docker-hub-api/)
[![](https://images.microbadger.com/badges/version/forumi0721alpinex64/alpine-x64-nodejs-docker-hub-api.svg)](https://microbadger.com/images/forumi0721alpinex64/alpine-x64-nodejs-docker-hub-api "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpinex64/alpine-x64-nodejs-docker-hub-api.svg)](https://microbadger.com/images/forumi0721alpinex64/alpine-x64-nodejs-docker-hub-api "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinex64/alpine-x64-nodejs-docker-hub-api.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-nodejs-docker-hub-api/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinex64/alpine-x64-nodejs-docker-hub-api.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-nodejs-docker-hub-api/)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64,armhf
* Appplication : [Node.js](https://nodejs-docker-hub-api.org/) [docker-hub-api](https://www.npmjs.com/package/docker-hub-api)
    - Node.js® is a JavaScript runtime built on Chrome's V8 JavaScript engine.
    - Docker Hub API is an API library written for NodeJS to access the official Docker Hub/Registry.
* Base Image
    - [forumi0721/alpine-x64-base](https://hub.docker.com/r/forumi0721/alpine-x64-base/)



----------------------------------------
#### Run

* x64
```sh
docker run -i -t --rm \
           -e USER_NAME=<username> \
           -e USER_PASSWD=<password> \
           forumi0721alpinex64/alpine-x64-nodejs-docker-hub-api:latest
```

* aarch64
```sh
docker run -i -t --rm \
           -e USER_NAME=<username> \
           -e USER_PASSWD=<password> \
           forumi0721alpineaarch64/alpine-aarch64-nodejs-docker-hub-api:latest
```



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| USER_NAME          | Docker hub login username                        |
| USER_PASSWD        | Docker hub login password                        |



----------------------------------------
* [forumi0721alpinex64/alpine-x64-nodejs-docker-hub-api](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-nodejs-docker-hub-api/)

